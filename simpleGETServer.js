const https = require('https')
const http = require('http')
const path = require('path')
const fs = require('fs')

const httpsPort = 443
const httpPort = 80
const options = {
 key: fs.readFileSync('example.com.key'),
 cert: fs.readFileSync('example.com.cert')
}

const index = "/index.html"
const generalHeaders = {
 "Cache-Control": "private, max-age=3600",
 "Content-Security-Policy": "upgrade-insecure-requests",
 "Strict-Transport-Security": "max-age=31536000; includeSubDomains",
 "Date": new Date().toUTCString()
}
const mimeTypes = {
 ".css": "text/css",
 ".csv": "text/csv",
 ".gz": "application/gzip",
 ".html": "text/html",
 ".jpg": "image/jpeg",
 ".js": "text/javascript",
 ".json": "application/json",
 ".mp3": "audio/mpeg",
 ".mjs": "text/javascript",
 ".ogg": "audio/ogg",
 ".png": "image/png",
 ".pdf": "application/pdf",
 ".svg": "image/svg+xml",
 ".tar": "application/x-tar",
 ".txt": "text/plain",
 ".xml": "text/xml",
 ".zip": "application/zip"
}

const acceptedMethods = {
 "GET": getMethod,
 "HEAD": headMethod
}

const httpsServer = https.createServer(options, (request, response) => {
 Object.keys(generalHeaders).forEach(key => {
  response.setHeader(key, generalHeaders[key])
 })
 if (acceptedMethods[request.method]) {
  let requestURL = path.normalize(request.url)
  if (requestURL == "/") requestURL = index
  const fileURL = process.cwd() + "/html"+ requestURL
  fs.promises.stat(fileURL).then(fileStats => {
   const lastModified = new Date(new Date(fileStats.mtime).toUTCString())
   const ifModifiedSince = new Date(request.headers["if-modified-since"])
   if (fileStats.isFile() && lastModified <= ifModifiedSince) {
    response.setHeader("Last-Modified", lastModified.toUTCString())
    response.statusCode = 304
    response.end()
   }
   else if (fileStats.isFile()) acceptedMethods[request.method](response, fileURL, fileStats, lastModified)
   else throw new Error("Not a file")
  }).catch(error => {
   response.statusCode = 404
   response.setHeader('Content-Type', 'text/html; charset=UTF-8')
   response.end("<html><meta charset='utf-8'/><body><h1>Error 404</h1><p>Not found</p></body></html>")
  })
 }
 else {
  response.statusCode = 405
  response.setHeader('Allow', Object.keys(acceptedMethods).join(", "))
  response.end()
 }
})

const httpServer = http.createServer((request, response) => {
 response.statusCode = 301
 response.setHeader('Location', "https://" + request.headers.host + path.normalize(request.url))
 response.end()
})

httpsServer.listen(httpsPort, () => {
 console.log("HTTPS Server Started")
 httpServer.listen(httpPort, () => {
  console.log("HTTP Server Started")
 })
})

function setFileHeaders(response, fileURL, fileStats, lastModified) {
 const fileExtension = path.extname(fileURL)
 let mimeType = "text/plain"
 if (mimeTypes[fileExtension]) mimeType = mimeTypes[fileExtension]
 response.setHeader("Content-Type", mimeType + "; charset=UTF-8")
 response.setHeader("Content-Length", fileStats.size)
 response.setHeader("Last-Modified", lastModified.toUTCString())
}

function getMethod(response, fileURL, fileStats, lastModified) {
 setFileHeaders(response, fileURL, fileStats, lastModified)
 fs.promises.readFile(fileURL).then(result => {
  response.statusCode = 200
  response.end(result)
 }).catch(error => {
  response.statusCode = 500
  response.end()
 })
}

function headMethod(response, fileURL, fileStats, lastModified) {
 setFileHeaders(response, fileURL, fileStats, lastModified)
 response.statusCode = 204
 response.end()
}
